﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace _5092_homework4
{
    public partial class Form1 : Form
    {
        double[] price = new double[10];//result for price
        double[] pricer = new double[4];// result for greekpublic string optiontype;
        public string Optiontype;
        System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
        public delegate void Getresult();
        public Getresult myresult;// delegate for get a result
        public delegate void test1();
        public test1 tes;

        public Form1()
        {
            InitializeComponent();
            myresult = new Getresult(Getresult_method);
           // tes = new test1(test1_method);

        }
        public  void Update(int i) // this for the progressive bar
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int>(Update), new object[] { i });
                return;
            }
            progressBar1.Value = i;
        }
       public void test1_method()
        {
           

                double num;
                if (!double.TryParse(Rebate.Text, out num))
                {
                    errorProvider1.SetError(Rebate, "please enter a positive  number");
                }
                else if (Convert.ToDouble(Rebate.Text) <= 0)
                {
                    errorProvider1.SetError(Rebate, "please enter a positive  number");
                }
                else
                {
                    errorProvider1.SetError(Rebate, string.Empty);
                }
            
        }
        public void Getresult_method()// this method is to pass result back to GUI
        {
            int cores = Environment.ProcessorCount;
            cores_n.Text = Convert.ToString(cores);
               
            call_price1.Text = Convert.ToString(pricer[0]);
            call_delta1.Text = Convert.ToString(price[0]);
            call_gamma1.Text = Convert.ToString(price[1]);
            call_theta1.Text = Convert.ToString(price[2]);
            call_rho1.Text = Convert.ToString(price[3]);
            call_vega1.Text = Convert.ToString(price[4]);
            put_price1.Text = Convert.ToString(pricer[2]);
            put_delta1.Text = Convert.ToString(price[5]);
            put_gamma1.Text = Convert.ToString(price[6]);
            put_theta1.Text = Convert.ToString(price[7]);
            put_rho1.Text = Convert.ToString(price[8]);
            put_vega1.Text = Convert.ToString(price[9]);
            call_se1.Text = Convert.ToString(pricer[1]);
            put_se1.Text = Convert.ToString(pricer[3]);
            watch.Stop();
            Update(100);
            runTime.Text = watch.Elapsed.Minutes.ToString() + " minutes "
                + watch.Elapsed.Seconds.ToString() + " seconds " + watch.Elapsed.Milliseconds.ToString() + " milliseconds ";
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        public static double[,] ran;
        private void button1_Click(object sender, EventArgs e)
        {
            
            watch.Reset();
            watch.Start();

            if (string.IsNullOrEmpty(optiontype.Text) == true)
            {
                errorProvider1.SetError(optiontype, "please choose the type of the option");
                return;
            }
            else
            {
                errorProvider1.SetError(optiontype, string.Empty);
            }

            Optiontype = Convert.ToString(optiontype.SelectedItem);
            if (Optiontype == "Digital option")
            {


                double num;
                if (!double.TryParse(Rebate.Text, out num))
                {
                    errorProvider1.SetError(Rebate, "please enter a positive  number");
                    return;
                }
                else if (Convert.ToDouble(Rebate.Text) <= 0)
                {
                    errorProvider1.SetError(Rebate, "please enter a positive  number");
                    return;
                }
                else
                {
                    errorProvider1.SetError(Rebate, string.Empty);
                }
            }

            string barriertype = Convert.ToString(Barriertype.SelectedItem);
            double S = Convert.ToDouble(Stockprice.Text);
            if (Optiontype == "Barrier option")
            {
                if (string.IsNullOrEmpty(Barriertype.Text) == true)
                {
                    errorProvider1.SetError(Barriertype, "please choose the type of the barriertype");
                    return;
                }
                else
                {
                    errorProvider1.SetError(Barriertype, string.Empty);
                }
                double num;
                if (!double.TryParse(BarrierLevel.Text, out num))
                {
                    errorProvider1.SetError(BarrierLevel, "please enter a positive  number");
                    return;
                }
                else if (Convert.ToDouble(BarrierLevel.Text) <= 0)
                {
                    errorProvider1.SetError(BarrierLevel, "please enter a positive  number");
                    return;
                }
                else
                {
                    errorProvider1.SetError(BarrierLevel, string.Empty);
                }
                if (barriertype == "up and out" || barriertype == "up and in")
                {
                    if (Convert.ToDouble(BarrierLevel.Text) < S)
                    {
                        errorProvider1.SetError(BarrierLevel, "please enter a positive  number which is larger than stock price");
                        return;
                    }
                    else
                    {
                        errorProvider1.SetError(BarrierLevel, string.Empty);
                    }
                }
                if (barriertype == "down and out" || barriertype == "down and in")
                {
                    if (Convert.ToDouble(BarrierLevel.Text) > S)
                    {
                        errorProvider1.SetError(BarrierLevel, "please enter a positive  number which is smaller than stock price");
                        return;
                    }
                    else
                    {
                        errorProvider1.SetError(BarrierLevel, string.Empty);
                    }
                }
            }
            int trials = Convert.ToInt32(Trials.Text);
            int N = Convert.ToInt32(steps.Text);
            
            double K = Convert.ToDouble(Kprice.Text);
            double r = Convert.ToDouble(rate.Text);
            double T = Convert.ToDouble(Tenor.Text);
            double sigma = Convert.ToDouble(vol.Text);
            //double rebate = Convert.ToDouble(Rebate.Text);
            double level = Convert.ToDouble(BarrierLevel.Text);
            

            /*double[] price=new double[4];
            double[] greek=new double[10];*/
            ran = new double[trials, N];
            bool anti, Deltac, muti;
            if (antithetic.Checked)
            {
                 anti = true;
            }
            else
            {
                anti = false;
            }
            if (delta_c.Checked)
            {
                Deltac = true;
            }
            else
            {
                Deltac = false;
            }
            if (mutithread.Checked)
            {
                muti = true;
            }
            else
            {
                muti = false;
            }
            Update(2);

            Action<object> cal = x =>// this is the main calculate program
            {
                Randompro simulater = new Randompro(trials, N);
                if (muti)
                {
                    simulater.multithread();
                }
                else
                {
                    simulater.randomnum();
                }
                Update(8);
                if (Optiontype == "European option")
                {
                    EurOption option1 = new EurOption(K, S, r, T, sigma, trials, N, Deltac, anti, muti);
                    
                    pricer = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(20);
                    double[] Price1 = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(30);
                    double[] Delta1 = option1.GetPrice(trials, N, 1.001 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(40);
                    double[] Gmma1 = option1.GetPrice(trials, N, 0.999 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(50);
                    double[] Theta1 = option1.GetPrice(trials, N, S, K, r, 1.001 * T, sigma, ran, anti, Deltac, muti); Update(60);
                    double[] Rho1 = option1.GetPrice(trials, N, S, K, 1.0001 * r, T, sigma, ran, anti, Deltac, muti); Update(70);
                    double[] Vega1 = option1.GetPrice(trials, N, S, K, r, T, 1.0001 * sigma, ran, anti, Deltac, muti); Update(80);
                    double callD = (Delta1[0] - Price1[0]) / (0.001 * S);
                    double callG = (Gmma1[0] + Delta1[0] - 2 * Price1[0]) / (0.001 * 0.001 * S * S);
                    double callT = -(Theta1[0] - Price1[0]) / (0.001 * T);
                    double callR = (Rho1[0] - Price1[0]) / (0.0001 * r);
                    double callV = (Vega1[0] - Price1[0]) / (0.0001 * sigma);
                    double putD = (Delta1[2] - Price1[2]) / (0.001 * S);
                    double putG = (Gmma1[2] + Delta1[2] - 2 * Price1[2]) / (0.001 * 0.001 * S * S);
                    double putT = -(Theta1[2] - Price1[2]) / (0.001 * T);
                    double putR = (Rho1[2] - Price1[2]) / (0.0001 * r);
                    double putV = (Vega1[2] - Price1[2]) / (0.0001 * sigma);



                    price[0] = callD;
                    price[1] = callG;
                    price[2] = callT;
                    price[3] = callR;
                    price[4] = callV;

                    price[5] = putD;
                    price[6] = putG;
                    price[7] = putT;
                    price[8] = putR;
                    price[9] = putV;
                }

                if (Optiontype == "Asian  option")
                {
                    AsianOption option1 = new AsianOption(K, S, r, T, sigma, trials, N, Deltac, anti, muti);

                    pricer = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(20);
                    double[] Price1 = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(30);
                    double[] Delta1 = option1.GetPrice(trials, N, 1.001 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(40);
                    double[] Gmma1 = option1.GetPrice(trials, N, 0.999 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(50);
                    double[] Theta1 = option1.GetPrice(trials, N, S, K, r, 1.001 * T, sigma, ran, anti, Deltac, muti); Update(60);
                    double[] Rho1 = option1.GetPrice(trials, N, S, K, 1.0001 * r, T, sigma, ran, anti, Deltac, muti); Update(70);
                    double[] Vega1 = option1.GetPrice(trials, N, S, K, r, T, 1.0001 * sigma, ran, anti, Deltac, muti); Update(80);
                    double callD = (Delta1[0] - Price1[0]) / (0.001 * S);
                    double callG = (Gmma1[0] + Delta1[0] - 2 * Price1[0]) / (0.001 * 0.001 * S * S);
                    double callT = -(Theta1[0] - Price1[0]) / (0.001 * T);
                    double callR = (Rho1[0] - Price1[0]) / (0.0001 * r);
                    double callV = (Vega1[0] - Price1[0]) / (0.0001 * sigma);
                    double putD = (Delta1[2] - Price1[2]) / (0.001 * S);
                    double putG = (Gmma1[2] + Delta1[2] - 2 * Price1[2]) / (0.001 * 0.001 * S * S);
                    double putT = -(Theta1[2] - Price1[2]) / (0.001 * T);
                    double putR = (Rho1[2] - Price1[2]) / (0.0001 * r);
                    double putV = (Vega1[2] - Price1[2]) / (0.0001 * sigma);



                    price[0] = callD;
                    price[1] = callG;
                    price[2] = callT;
                    price[3] = callR;
                    price[4] = callV;

                    price[5] = putD;
                    price[6] = putG;
                    price[7] = putT;
                    price[8] = putR;
                    price[9] = putV;
                }
                if (Optiontype == "Digital option")
                {


                   

                    double rebate = Convert.ToDouble(Rebate.Text);
                    DigitalOption option1 = new DigitalOption(K, S, r, T, sigma, trials, N,rebate, Deltac, anti, muti);

                    pricer = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(20);
                    double[] Price1 = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(30);
                    double[] Delta1 = option1.GetPrice(trials, N, 1.001 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(40);
                    double[] Gmma1 = option1.GetPrice(trials, N, 0.999 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(50);
                    double[] Theta1 = option1.GetPrice(trials, N, S, K, r, 1.001 * T, sigma, ran, anti, Deltac, muti); Update(60);
                    double[] Rho1 = option1.GetPrice(trials, N, S, K, 1.0001 * r, T, sigma, ran, anti, Deltac, muti); Update(70);
                    double[] Vega1 = option1.GetPrice(trials, N, S, K, r, T, 1.001 * sigma, ran, anti, Deltac, muti); Update(80);
                    double callD = (Delta1[0] - Price1[0]) / (0.001 * S);
                    double callG = (Gmma1[0] + Delta1[0] - 2 * Price1[0]) / (0.001 * 0.001 * S * S);
                    double callT = -(Theta1[0] - Price1[0]) / (0.001 * T);
                    double callR = (Rho1[0] - Price1[0]) / (0.0001 * r);
                    double callV = (Vega1[0] - Price1[0]) / (0.001 * sigma);
                    double putD = (Delta1[2] - Price1[2]) / (0.001 * S);
                    double putG = (Gmma1[2] + Delta1[2] - 2 * Price1[2]) / (0.001 * 0.001 * S * S);
                    double putT = -(Theta1[2] - Price1[2]) / (0.001 * T);
                    double putR = (Rho1[2] - Price1[2]) / (0.0001 * r);
                    double putV = (Vega1[2] - Price1[2]) / (0.001 * sigma);



                    price[0] = callD;
                    price[1] = callG;
                    price[2] = callT;
                    price[3] = callR;
                    price[4] = callV;

                    price[5] = putD;
                    price[6] = putG;
                    price[7] = putT;
                    price[8] = putR;
                    price[9] = putV;
                }
                if (Optiontype == "Barrier option")
                {
                    /*if (string.IsNullOrEmpty(Barriertype.Text) == true)
                    {
                        errorProvider1.SetError(optiontype, "please choose the type of the barriertype");
                        return;
                    }
                    else
                    {
                        errorProvider1.SetError(Barriertype, string.Empty);
                    }
                    double num;
                    if (!double.TryParse(BarrierLevel.Text, out num))
                    {
                        errorProvider1.SetError(BarrierLevel, "please enter a positive  number");
                    }
                    else if (Convert.ToDouble(BarrierLevel.Text) <= 0)
                    {
                        errorProvider1.SetError(BarrierLevel, "please enter a positive  number");
                    }
                    else
                    {
                        errorProvider1.SetError(BarrierLevel, string.Empty);
                    }
                    if (barriertype== "up and out"||barriertype=="up and in")
                    {
                        if (level<S)
                        {
                            errorProvider1.SetError(BarrierLevel, "please enter a positive  number which is larger than stock price");
                        }
                        else
                        {
                            errorProvider1.SetError(BarrierLevel, string.Empty);
                        }
                    }
                    if (barriertype == "down and out" || barriertype == "down and in")
                    {
                        if (level > S)
                        {
                            errorProvider1.SetError(BarrierLevel, "please enter a positive  number which is larger than stock price");
                        }
                        else
                        {
                            errorProvider1.SetError(BarrierLevel, string.Empty);
                        }
                    }
                    */
                    BarrierOption option1 = new BarrierOption(K, S, r, T, sigma, trials, N,level,barriertype, Deltac, anti, muti);

                    pricer = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(20);
                    double[] Price1 = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(30);
                    double[] Delta1 = option1.GetPrice(trials, N, 1.001 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(40);
                    double[] Gmma1 = option1.GetPrice(trials, N, 0.999 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(50);
                    double[] Theta1 = option1.GetPrice(trials, N, S, K, r, 1.001 * T, sigma, ran, anti, Deltac, muti); Update(60);
                    double[] Rho1 = option1.GetPrice(trials, N, S, K, 1.0001 * r, T, sigma, ran, anti, Deltac, muti); Update(70);
                    double[] Vega1 = option1.GetPrice(trials, N, S, K, r, T, 1.0001 * sigma, ran, anti, Deltac, muti); Update(80);
                    double callD = (Delta1[0] - Price1[0]) / (0.001 * S);
                    double callG = (Gmma1[0] + Delta1[0] - 2 * Price1[0]) / (0.001 * 0.001 * S * S);
                    double callT = -(Theta1[0] - Price1[0]) / (0.001 * T);
                    double callR = (Rho1[0] - Price1[0]) / (0.0001 * r);
                    double callV = (Vega1[0] - Price1[0]) / (0.0001 * sigma);
                    double putD = (Delta1[2] - Price1[2]) / (0.001 * S);
                    double putG = (Gmma1[2] + Delta1[2] - 2 * Price1[2]) / (0.001 * 0.001 * S * S);
                    double putT = -(Theta1[2] - Price1[2]) / (0.001 * T);
                    double putR = (Rho1[2] - Price1[2]) / (0.0001 * r);
                    double putV = (Vega1[2] - Price1[2]) / (0.0001 * sigma);



                    price[0] = callD;
                    price[1] = callG;
                    price[2] = callT;
                    price[3] = callR;
                    price[4] = callV;

                    price[5] = putD;
                    price[6] = putG;
                    price[7] = putT;
                    price[8] = putR;
                    price[9] = putV;
                }
                if (Optiontype == "Lookback option")
                {
                    LookbackOption option1 = new LookbackOption(K, S, r, T, sigma, trials, N, Deltac, anti, muti);

                    pricer = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(20);
                    double[] Price1 = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(30);
                    double[] Delta1 = option1.GetPrice(trials, N, 1.001 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(40);
                    double[] Gmma1 = option1.GetPrice(trials, N, 0.999 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(50);
                    double[] Theta1 = option1.GetPrice(trials, N, S, K, r, 1.001 * T, sigma, ran, anti, Deltac, muti); Update(60);
                    double[] Rho1 = option1.GetPrice(trials, N, S, K, 1.0001 * r, T, sigma, ran, anti, Deltac, muti); Update(70);
                    double[] Vega1 = option1.GetPrice(trials, N, S, K, r, T, 1.0001 * sigma, ran, anti, Deltac, muti); Update(80);
                    double callD = (Delta1[0] - Price1[0]) / (0.001 * S);
                    double callG = (Gmma1[0] + Delta1[0] - 2 * Price1[0]) / (0.001 * 0.001 * S * S);
                    double callT = -(Theta1[0] - Price1[0]) / (0.001 * T);
                    double callR = (Rho1[0] - Price1[0]) / (0.0001 * r);
                    double callV = (Vega1[0] - Price1[0]) / (0.0001 * sigma);
                    double putD = (Delta1[2] - Price1[2]) / (0.001 * S);
                    double putG = (Gmma1[2] + Delta1[2] - 2 * Price1[2]) / (0.001 * 0.001 * S * S);
                    double putT = -(Theta1[2] - Price1[2]) / (0.001 * T);
                    double putR = (Rho1[2] - Price1[2]) / (0.0001 * r);
                    double putV = (Vega1[2] - Price1[2]) / (0.0001 * sigma);



                    price[0] = callD;
                    price[1] = callG;
                    price[2] = callT;
                    price[3] = callR;
                    price[4] = callV;

                    price[5] = putD;
                    price[6] = putG;
                    price[7] = putT;
                    price[8] = putR;
                    price[9] = putV;
                }
                if (Optiontype == "Range option ")
                {
                    RangeOption option1 = new RangeOption(K, S, r, T, sigma, trials, N, Deltac, anti, muti);

                    pricer = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(20);
                    double[] Price1 = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, Deltac, muti); Update(30);
                    double[] Delta1 = option1.GetPrice(trials, N, 1.001 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(40);
                    double[] Gmma1 = option1.GetPrice(trials, N, 0.999 * S, K, r, T, sigma, ran, anti, Deltac, muti); Update(50);
                    double[] Theta1 = option1.GetPrice(trials, N, S, K, r, 1.001 * T, sigma, ran, anti, Deltac, muti); Update(60);
                    double[] Rho1 = option1.GetPrice(trials, N, S, K, 1.0001 * r, T, sigma, ran, anti, Deltac, muti); Update(70);
                    double[] Vega1 = option1.GetPrice(trials, N, S, K, r, T, 1.0001 * sigma, ran, anti, Deltac, muti); Update(80);
                    double callD = (Delta1[0] - Price1[0]) / (0.001 * S);
                    double callG = (Gmma1[0] + Delta1[0] - 2 * Price1[0]) / (0.001 * 0.001 * S * S);
                    double callT = -(Theta1[0] - Price1[0]) / (0.001 * T);
                    double callR = (Rho1[0] - Price1[0]) / (0.0001 * r);
                    double callV = (Vega1[0] - Price1[0]) / (0.0001 * sigma);
                    double putD = (Delta1[2] - Price1[2]) / (0.001 * S);
                    double putG = (Gmma1[2] + Delta1[2] - 2 * Price1[2]) / (0.001 * 0.001 * S * S);
                    double putT = -(Theta1[2] - Price1[2]) / (0.001 * T);
                    double putR = (Rho1[2] - Price1[2]) / (0.0001 * r);
                    double putV = (Vega1[2] - Price1[2]) / (0.0001 * sigma);



                    price[0] = callD;
                    price[1] = callG;
                    price[2] = callT;
                    price[3] = callR;
                    price[4] = callV;

                    price[5] = putD;
                    price[6] = putG;
                    price[7] = putT;
                    price[8] = putR;
                    price[9] = putV;
                }
                Update(90);
                Program.GUI.BeginInvoke(Program.GUI.myresult);
                
            };
            Thread a = new Thread(new ParameterizedThreadStart(cal));// this is another thread for defreezing the GUI
            a.Start();
            /*call_price1.Text = Convert.ToString(price[0]);
            call_delta1.Text = Convert.ToString(greek[0]);
            call_gamma1.Text = Convert.ToString(greek[1]);
            call_theta1.Text = Convert.ToString(greek[2]);
            call_rho1.Text = Convert.ToString(greek[3]);
            call_vega1.Text = Convert.ToString(greek[4]);
            put_price1.Text = Convert.ToString(price[2]);
            put_delta1.Text = Convert.ToString(greek[5]);
            put_gamma1.Text = Convert.ToString(greek[6]);
            put_theta1.Text = Convert.ToString(greek[7]);
            put_rho1.Text = Convert.ToString(greek[8]);
            put_vega1.Text = Convert.ToString(greek[9]);
            call_se1.Text = Convert.ToString(price[1]);
            put_se1.Text = Convert.ToString(price[3]);
            
            watch.Stop();
            runTime.Text = watch.Elapsed.Minutes.ToString() + " minutes "
                + watch.Elapsed.Seconds.ToString() + " seconds " + watch.Elapsed.Milliseconds.ToString() + " milliseconds ";*/
           
        }

        private void antithetic_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Stockprice_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(Stockprice.Text, out num))
            {
                errorProvider1.SetError(Stockprice, "please enter a positive int number");
            }
            else if (Convert.ToDouble(Stockprice.Text) <= 0)
            {
                errorProvider1.SetError(Stockprice, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(Stockprice, string.Empty);
            }
        }

        private void Kprice_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(Kprice.Text, out num))
            {
                errorProvider1.SetError(Kprice, "please enter a positive int number");
            }
            else if (Convert.ToDouble(Kprice.Text) <= 0)
            {
                errorProvider1.SetError(Kprice, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(Kprice, string.Empty);
            }
        }

        private void rate_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(rate.Text, out num))
            {
                errorProvider1.SetError(rate, "please enter a positive int number");
            }
            else if (Convert.ToDouble(rate.Text) <= 0)
            {
                errorProvider1.SetError(rate, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(rate, string.Empty);
            }
        }

        private void vol_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(vol.Text, out num))
            {
                errorProvider1.SetError(vol, "please enter a positive int number");
            }
            else if (Convert.ToDouble(vol.Text) <= 0)
            {
                errorProvider1.SetError(vol, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(vol, string.Empty);
            }
        }

        private void Tenor_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(Tenor .Text, out num))
            {
                errorProvider1.SetError(Tenor, "please enter a positive int number");
            }
            else if (Convert.ToDouble(Tenor.Text) <= 0)
            {
                errorProvider1.SetError(Tenor, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(Tenor, string.Empty);
            }
        }

        private void Trials_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(Trials.Text, out num))
            {
                errorProvider1.SetError(Trials, "please enter a positive int number");
            }
            else if (Convert.ToDouble(Trials.Text) <= 0)
            {
                errorProvider1.SetError(Trials, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(Trials, string.Empty);
            }
        }

        private void steps_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(steps.Text, out num))
            {
                errorProvider1.SetError(steps, "please enter a positive int number");
            }
            else if (Convert.ToDouble(steps.Text) <= 0)
            {
                errorProvider1.SetError(steps, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(steps, string.Empty);
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            if (Optiontype == "Digital option")
            {


                double num;
                if (!double.TryParse(Rebate.Text, out num))
                {
                    errorProvider1.SetError(Rebate, "please enter a positive  number");
                }
                else if (Convert.ToDouble(Rebate.Text) <= 0)
                {
                    errorProvider1.SetError(Rebate, "please enter a positive  number");
                }
                else
                {
                    errorProvider1.SetError(Rebate, string.Empty);
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void BarrierLevel_TextChanged(object sender, EventArgs e)
        {
            
                double num;
                if (!double.TryParse(Rebate.Text, out num))
                {
                    errorProvider1.SetError(Rebate, "please enter a positive  number");
                }
                else if (Convert.ToDouble(Rebate.Text) <= 0)
                {
                    errorProvider1.SetError(Rebate, "please enter a positive  number");
                }
                else
                {
                    errorProvider1.SetError(Rebate, string.Empty);
                }
            
        }
    }
}
